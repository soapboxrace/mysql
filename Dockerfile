FROM mysql:5.7.37

COPY soapbox-full.sql.gz /docker-entrypoint-initdb.d

EXPOSE 3306

ENV MYSQL_ROOT_PASSWORD=example
ENV MYSQL_DATABASE=SOAPBOX
ENV MYSQL_USER=soapbox
ENV MYSQL_PASSWORD=soapbox
